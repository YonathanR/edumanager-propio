package com.solucionesdigitales.edumanagersite;

import com.solucionesdigitales.edumanagersite.entidades.Archivos;
import com.solucionesdigitales.edumanagersite.servicios.ArchivosService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = EduManagerSiteApplication.class,loader = AnnotationConfigContextLoader.class)
public class EduManagerSiteApplicationTests
{
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("ArchivosService")
    private ArchivosService service;

    @Test
    public void agregarArchivo()
    {
        try
        {
            Archivos archivo = new Archivos("Audio",".mp3, .OGG, .AC3", "TODOS", "Ciencias", "Historia", "Yonathan Roman", "Audio, historico, segunda, guerra", Date.valueOf("2018-08-23"), "Tercera", "Libro de historia sobre la segunda guerra mundial");
            if (service.agregarArchivo(archivo))
            {
                log.info("Se agrego el archivo correctamente, archivo: ");
            }
            else
            {
                log.warn("No fue posible agregar el archivo ");
            }
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
        }
    }


    @Test
    public void borrarArchivo()
    {
        try
        {
            Archivos archivo = service.obtenerArchivo(1);
            if (service.borrarArchivo(archivo))
            {
                log.info("Se borro el archivo correctamente, archivo:\n"+archivo.toString());
            }
            else
            {
                log.warn("No fue posible borrar el archivo ");
            }
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
        }
    }

    @Test
    public void actualizarArchivo()
    {
        try
        {
            Archivos archivo = new Archivos(6,"Audio",".mp3, .OGG, .AC3", "NADIE", "Guerra mundial", "Historia", "Yonathan Roman", "Audio, historico, segunda, guerra", Date.valueOf("2018-08-23"), "Tercera", "Libro de historia sobre la segunda guerra mundial");

            if (service.obtenerArchivo(6)!= null)
            {
                service.actualizarArchivo(archivo);
                log.info("Se actualizo el archivo correctamente, archivo:\n"+archivo.toString());
            }
            else
            {
                log.warn("No fue posible actualizar el archivo ");
            }
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
        }
    }

    @Test
    public void Listar()
    {
        try
        {
            List<Archivos> archivos= new ArrayList<>();
            archivos = service.listaArchivos();
            for (Archivos archivo: archivos)
            {
                log.info(archivo.toString());
            }
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
        }
    }
}
