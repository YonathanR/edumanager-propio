dashboard.controller("MenuRegistroContenidoController", function ($scope,$document, $http)
{
    $scope.accion = "";

    $scope.Seccion = function (accion)
    {
        return accion == $scope.accion;
    };

    $scope.verSeccion = function (accion)
    {
        $scope.accion = accion;
    };

});