var dashboard = angular.module("EduManagerDashboardApp", ["ngRoute","ngTagsInput"], function ($compileProvider)
{
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
});

dashboard.config(['$routeProvider','$locationProvider', function ($routeProvider,$locationProvider)
{
    $locationProvider.hashPrefix('');
    $routeProvider
          .when("/", {
            templateUrl: "/vistas/dashboard/principal.html",
              controller: "principalController"
        }).when("/menucontenido", {
            templateUrl: "/vistas/dashboard/MenuRegistroContenido.html",
            controller: "MenuRegistroContenidoController"
        }).when("/registrocontenido/:tipoContenido", {
            templateUrl: "/vistas/dashboard/registroContenido.html",
            controller: "registrarContenidoController"
        }).otherwise({
            templateUrl: "/"

    });
}]);

dashboard.config(function (tagsInputConfigProvider)
{
    tagsInputConfigProvider
        .setDefaults('tagsInput',{
            placeholder: 'Agregar etiquetas',
            minLength:2,
            addOnEnter:true,
            addOnSpace:false,
            addOnComa:true,
            // removeTagSymbol:"-",
            replaceSpacesWithDashes:false,
            maxTags:100
        })
});

dashboard.controller("dashboardController", function ($scope,$document, $http, $location)
{
    $scope.cambiarVista = function (ruta)
    {
        $location.path(ruta);
    };

    $scope.minombre = "Yonathan Roman Salgado";
    angular.element($document).ready(function ()
    {
        $scope.Aviso = function ()
        {
            // $.toast().reset('all');
            $("body").removeAttr('class');
            $.toast({
                heading: '¡Oops! Esto aun esta en desarrollo',
                text: 'Intente pasado un tiempo o contacte al administrador',
                position: 'bottom-right',
                loaderBg:'#ff2a00',
                icon: 'warning',
                hideAfter: 4000,
                stack: 6
            });
        };
    });
});