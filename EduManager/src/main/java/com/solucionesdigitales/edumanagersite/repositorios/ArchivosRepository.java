package com.solucionesdigitales.edumanagersite.repositorios;

import com.solucionesdigitales.edumanagersite.entidades.Archivos;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository ("ArchivosRepository")
public interface ArchivosRepository extends CrudRepository<Archivos, Serializable>
{
    @Query(value = "SELECT * FROM Archivos WHERE Tipo = :tipo",nativeQuery = true)
    public List<Archivos> listarArchivosPorTipo(@Param("tipo") String tipoArchivo);

    @Query(value = "SELECT count(tipo) FROM Archivos WHERE Tipo= :tipo",nativeQuery = true)
    public int CantidadDeCadaTipo(@Param("tipo") String CantidadDeContenido);
}
