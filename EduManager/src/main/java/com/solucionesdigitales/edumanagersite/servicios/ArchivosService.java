package com.solucionesdigitales.edumanagersite.servicios;

import com.solucionesdigitales.edumanagersite.entidades.Archivos;

import java.util.List;

public interface ArchivosService
{
    public boolean agregarArchivo(Archivos archivo);
    public boolean actualizarArchivo(Archivos archivo);
    public boolean borrarArchivo(Archivos archivo);
    public Archivos obtenerArchivo(int archivoID);
    public List<Archivos> listaArchivos();
    public List<Archivos> listarArchivosPorTipo(String tipoArchivo);
    public int CantidadDeArchivos(String tipo);

}
