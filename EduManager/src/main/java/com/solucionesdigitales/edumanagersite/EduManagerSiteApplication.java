package com.solucionesdigitales.edumanagersite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = ("com.solucionesdigitales"))
public class EduManagerSiteApplication
{

    public static void main(String[] args) {
        SpringApplication.run(EduManagerSiteApplication.class, args);
    }
}
