package com.solucionesdigitales.edumanagersite.controladores;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.solucionesdigitales.edumanagersite.entidades.Archivos;
import com.solucionesdigitales.edumanagersite.servicios.ArchivosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "/")
public class BibliotecaController
{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,false);

    @Autowired
    @Qualifier("ArchivosService")
    private ArchivosService archivosService;

    @RequestMapping(path = "archivos", method = RequestMethod.POST)
    public @ResponseBody boolean registrarArchivo(@RequestBody String archivoJSON)
    {
        try
        {
            Archivos archivo = new Archivos();

            archivo = mapper.readValue(archivoJSON, Archivos.class);
            log.info("Se recibio del formulario: \n"+archivo.toString());

            if (archivosService.agregarArchivo(archivo))
            {
                log.info("Se cargo el archivo correctamente.");
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }

    @RequestMapping(path = "archivos/{tipo}", method = RequestMethod.GET)
    public @ResponseBody List<Archivos>listaArchivosPorTipo(@PathVariable("tipo") String tipoArchivo)
    {
        try
        {
            if (tipoArchivo != null)
            {
                log.info("Se recibe en controller: "+tipoArchivo);
                return archivosService.listarArchivosPorTipo(tipoArchivo);
            }
            log.warn("El tipo llega vacio");
            return null;
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
            return null;
        }
    }

    @RequestMapping(path = "archivos",method = RequestMethod.DELETE)
    public @ResponseBody boolean borrarArchivo(@RequestBody String archivoJSON)
    {
        try
        {
            if (archivoJSON != null)
            {
                Archivos archivo = mapper.readValue(archivoJSON, Archivos.class);
                return archivosService.borrarArchivo(archivo);
            }
            log.warn("El archivo llega vacio");
            return false;
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
            return false;
        }
    }

    @RequestMapping(path = "archivos", method = RequestMethod.PUT)
    public @ResponseBody boolean actualizarArchivo(@RequestBody String archivoJSON)
    {
        try
        {
            Archivos archivo = new Archivos();

            archivo = mapper.readValue(archivoJSON, Archivos.class);
            log.info("Se recibio del formulario: \n"+archivo.toString());

            if (archivosService.actualizarArchivo(archivo))
            {
                log.info("Se actualizo el archivo correctamente.");
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }
}
